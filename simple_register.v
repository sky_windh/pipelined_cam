`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/09/2017 02:43:35 PM
// Design Name: 
// Module Name: simple_register
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module simple_register #(
   parameter WIDTH            = 8,
   parameter DEPTH            = 3
) (
   input                      clk,
   input    [WIDTH-1:0]       data_in,
   output   [WIDTH-1:0]       data_out
);

   reg   [WIDTH-1:0]          buffer_s[DEPTH-1:0];
   integer init;
   initial for (init=0; init<DEPTH; init=init+1) buffer_s[init] = {WIDTH{1'b0}};

   // Assign the input
   always @(posedge clk)
      buffer_s[DEPTH-1] <= data_in;

   // Assign the output
   assign data_out = buffer_s[0];

   // Make the register chain
   genvar i;
   for (i = 0; i < DEPTH-1; i = i + 1)
   begin
      always @(posedge clk)
         buffer_s[i] <= buffer_s[i+1];
   end

endmodule

