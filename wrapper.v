`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/09/2017 02:40:44 PM
// Design Name: 
// Module Name: wrapper
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module wrapper  #(
   parameter WIDTH            = 32,
   parameter STAGES            = 2
)(
    input                      clk,
    output                     ready,
    
    input                      A_write_en_in,
    input    [WIDTH-1:0]       A_data_in,
    input    [5:0]             A_addr_in,
    
    input                      B_read_en_in,
    input    [WIDTH-1:0]       B_data_in,
    output                     B_valid_out,
    output                     B_match_out,
    output   [WIDTH-1:0]       B_data_out,
    output   [5:0]             B_addr_out
);


    wire                      A_write_en_s;
    wire    [WIDTH-1:0]       A_data_s;
    wire    [5:0]             A_addr_s;
    
    wire                      B_read_en_s;
    wire    [WIDTH-1:0]       B_data_in_s;
    wire                     B_valid_s;
    wire                     B_match_s;
    wire   [WIDTH-1:0]       B_data_out_s;
    wire   [5:0]             B_addr_s;

    //io pipelines
   simple_register #(
       .WIDTH                  (1),
       .DEPTH                  (3)
    ) A_WRITE_CHAIN (
       .clk                    (clk),
       .data_in                (A_write_en_in),
       .data_out               (A_write_en_s)
    );

   simple_register #(
      .WIDTH                  (WIDTH),
      .DEPTH                  (3)
   ) A_DATA_CHAIN (
      .clk                    (clk),
      .data_in                (A_data_in),
      .data_out               (A_data_s)
   );

   simple_register #(
      .WIDTH                  (6),
      .DEPTH                  (3)
   ) A_ADDR_CHAIN (
      .clk                    (clk),
      .data_in                (A_addr_in),
      .data_out               (A_addr_s)
   );

   simple_register #(
      .WIDTH                  (1),
      .DEPTH                  (3)
   ) B_RD_CHAIN (
      .clk                    (clk),
      .data_in                (B_read_en_in),
      .data_out               (B_read_en_s)
   );
   
   simple_register #(
      .WIDTH                  (WIDTH),
      .DEPTH                  (3)
   ) B_DATA_IN_CHAIN (
      .clk                    (clk),
      .data_in                (B_data_in),
      .data_out               (B_data_in_s)
   );
   
   simple_register #(
      .WIDTH                  (1),
      .DEPTH                  (3)
   ) B_VLD_CHAIN (
      .clk                    (clk),
      .data_in                (B_valid_s),
      .data_out               (B_valid_out)
   );
   
   simple_register #(
      .WIDTH                  (1),
      .DEPTH                  (3)
   ) B_MATCH_CHAIN (
      .clk                    (clk),
      .data_in                (B_match_s),
      .data_out               (B_match_out)
   );
      
     simple_register #(
        .WIDTH                  (WIDTH),
        .DEPTH                  (3)
     ) B_DATA_OUT_CHAIN (
        .clk                    (clk),
        .data_in                (B_data_out_s),
        .data_out               (B_data_out)
     );
  
     simple_register #(
        .WIDTH                  (6),
        .DEPTH                  (3)
     ) B_ADDR_CHAIN (
        .clk                    (clk),
        .data_in                (B_addr_s),
        .data_out               (B_addr_out)
     );

    
        cam_2k#(
             .WIDTH(64)
         ) uut (
        .clk(clk),
        .ready(ready),
        
        .A_write_en_in(A_write_en_in),
        .A_data_in(A_data_s),
        .A_addr_in(A_addr_s),
        
        .B_read_en_in(B_read_en_s),
        .B_data_in(B_data_s),
        .B_valid_out(B_valid_s),
        .B_match_out(B_match_s),
        .B_data_out(B_data_s),
        .B_addr_out(B_addr_s)
    );
endmodule
