`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/04/2017 02:31:20 PM
// Design Name: 
// Module Name: pipelined_cam_64_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pipelined_cam_64_tb();

    //inputs 
    reg clk;
    reg        A_write_en_in;
    reg [31:0] A_data_in;
    reg [5:0]  A_addr_in;
    
    reg        B_read_en_in;
    reg [31:0] B_data_in;
    
    
    //outputs
    wire        ready;
    wire        B_valid_out;
    wire        B_match_out;
    wire [31:0] B_data_out;
    wire [5:0]  B_addr_out;
             
    always 
        #5 clk = ~clk;
    
    pipelined_cam_64 uut (
        .clk(clk),
        .ready(ready),
        
        .A_write_en_in(A_write_en_in),
        .A_data_in(A_data_in),
        .A_addr_in(A_addr_in),
        
        .B_read_en_in(B_read_en_in),
        .B_data_in(B_data_in),
        .B_valid_out(B_valid_out),
        .B_match_out(B_match_out),
        .B_data_out(B_data_out),
        .B_addr_out(B_addr_out)
    );
    
    initial begin
        $display($time, " << Starting Simulation >>");
        clk = 1'b0;
        A_write_en_in = 1'b0;
        B_read_en_in = 1'b0;
        A_data_in = 'd0;     
        A_addr_in = 'd0;     
        B_data_in = 'd0;
        
        #105;
        
        A_write_en_in = 1'b1;
        A_data_in = 32'd1;
        A_addr_in = 6'd2;
        
        #10;
        
        A_data_in = 32'd5;
        A_addr_in = 6'd10;
        
        #10;
        
        A_data_in = 32'd7;
        A_addr_in = 6'd14;
        
        #10;
        
        
        A_data_in = 32'd20;
        A_addr_in = 6'd40;
        
        #10;
         
        A_write_en_in = 1'b0;
        
        #50;
        B_read_en_in = 1'b1;
        B_data_in = 6'd14;
        
        #10;
        B_data_in = 6'd40;
        
        #10;
        B_data_in = 6'd20;
        
        #10;
        B_data_in = 6'd5;
        
        #10;        
        B_data_in = 6'd1;
        
        #10;
        B_read_en_in = 1'b0;
        $finish;
        end
endmodule
