`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/04/2017 02:24:32 PM
// Design Name: 
// Module Name: pipelined_cam_64
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module pipelined_cam_64  #(
   parameter WIDTH            = 32,
   parameter STAGES            = 2
)(
   input                      clk,
   output                     ready,

   input                      A_write_en_in,
   input    [WIDTH-1:0]       A_data_in,
   input    [5:0]             A_addr_in,

   input                      B_read_en_in,
   input    [WIDTH-1:0]       B_data_in,
   output                     B_valid_out,
   output                     B_match_out,
   output   [WIDTH-1:0]       B_data_out,
   output   [5:0]             B_addr_out
);

   reg   [WIDTH/STAGES-1:0]               data_buffer[0:63][0:STAGES-1];
   integer init, s;
   initial begin
      for (s=0; s<STAGES; s=s+1) begin
        for (init=0; init<64; init=init+1) begin
            data_buffer[init][s] = {(WIDTH/STAGES){1'b1}};
        end
      end
   end


   reg [STAGES:0] A_write_en;
   reg [WIDTH-1:0] A_write_data [0:STAGES];
   reg [47:0] A_write_addr [0:STAGES];
   genvar i;
   generate for(i=0; i <STAGES; i=i+1) begin : write_stage
        always @(posedge clk) begin
           if( i == 0) begin
                A_write_en[i]   <= A_write_en_in;
                A_write_data[i] <= A_data_in;
                A_write_addr[i] <= A_addr_in;
           end 
                A_write_en[i+1]   <= A_write_en[i];
                A_write_data[i+1] <= A_write_data[i];
                A_write_addr[i+1] <= A_write_addr[i];
           if (A_write_en[i]) begin
              data_buffer[A_write_addr[i]][i] <= A_write_data[i][(WIDTH/STAGES)*i+:(WIDTH/STAGES)];
           end
        end
   end endgenerate

   integer                    m;
   reg                        B_valid_c[0:STAGES];
   reg                        B_valid_r[0:STAGES];
   reg   [WIDTH-1:0]          B_data_out_c[0:STAGES];
   reg   [WIDTH-1:0]          B_data_out_r[0:STAGES];
   reg   [63:0]               B_match_c[0:STAGES];
   reg   [63:0]               B_match_r[0:STAGES];
   wire  [5:0]                binary_addr;

   encoder64 encoder(
       .encoder_in(B_match_r[STAGES]),
       .binary_out(binary_addr)
   );
   generate for(i=0; i<=STAGES; i=i+1) begin : read_stage
        always @* begin
           if( i == 0 ) begin
                B_match_c[0] = 2**64 - 1; //null case is match
                B_valid_c[0] = B_read_en_in; //null case is match
                B_data_out_c[0] = B_data_in; //null case is match
           end else begin
               B_valid_c[i] = B_valid_r[i-1];
               B_data_out_c[i] = B_data_out_r[i-1];
               for (m = 0; m < 64; m = m +1) begin
                    if( B_match_r[i-1][m] && B_valid_r[i-1]) begin
                        B_match_c[i][m] = data_buffer[m][i-1] == B_data_out_r[i-1][(WIDTH/STAGES)*(i-1)+:(WIDTH/STAGES)];
                    end else begin
                        B_match_c[i][m] = 1'b0;
                    end
               end
           end
        end

        always @(posedge clk) begin
           B_valid_r[i]      <= B_valid_c[i];
           B_data_out_r[i]   <= B_data_out_c[i];
           B_match_r[i]      <= B_match_c[i];
        end
   end endgenerate

   assign ready         = 1;

   assign B_valid_out   = B_valid_r[STAGES];
   assign B_match_out   = |B_match_r[STAGES];
   assign B_data_out    = B_data_out_r[STAGES];
   assign B_addr_out    = binary_addr;

endmodule