`timescale 1ns / 1ps

module cam_512 #(
   parameter WIDTH            = 32
)(
   input                      clk,
   output                     ready,
   
   input                      A_write_en_in,
   input    [WIDTH-1:0]            A_data_in,
   input    [8:0]             A_addr_in,
   
   input                      B_read_en_in,
   input    [WIDTH-1:0]            B_data_in,
   output                     B_valid_out,
   output                     B_match_out,
   output   [WIDTH-1:0]            B_data_out,
   output   [8:0]             B_addr_out
);

   wire                       cam_0_ready_s;
   wire                       cam_0_a_write_en_s;
   wire                       cam_0_b_valid_s;
   wire                       cam_0_b_match_s;
   wire  [WIDTH-1:0]               cam_0_b_data_s;
   wire  [7:0]                cam_0_b_addr_s;

   assign cam_0_a_write_en_s = A_write_en_in && !A_addr_in[8];
   cam_256 #(
		.WIDTH(WIDTH)
	) CAM_0 (
      .clk                    (clk),
      .ready                  (cam_0_ready_s),
      .A_write_en_in          (cam_0_a_write_en_s),
      .A_data_in              (A_data_in),
      .A_addr_in              (A_addr_in[7:0]),
      .B_read_en_in           (B_read_en_in),
      .B_data_in              (B_data_in),
      .B_valid_out            (cam_0_b_valid_s),
      .B_match_out            (cam_0_b_match_s),
      .B_data_out             (cam_0_b_data_s),
      .B_addr_out             (cam_0_b_addr_s)
   );

   wire                       cam_1_ready_s;
   wire                       cam_1_a_write_en_s;
   wire                       cam_1_b_valid_s;
   wire                       cam_1_b_match_s;
   wire  [WIDTH-1:0]               cam_1_b_data_s;
   wire  [7:0]                cam_1_b_addr_s;

   assign cam_1_a_write_en_s = A_write_en_in && A_addr_in[8];
   cam_256 #(
		.WIDTH(WIDTH)
	) CAM_1 (
      .clk                    (clk),
      .ready                  (cam_1_ready_s),
      .A_write_en_in          (cam_1_a_write_en_s),
      .A_data_in              (A_data_in),
      .A_addr_in              (A_addr_in[7:0]),
      .B_read_en_in           (B_read_en_in),
      .B_data_in              (B_data_in),
      .B_valid_out            (cam_1_b_valid_s),
      .B_match_out            (cam_1_b_match_s),
      .B_data_out             (cam_1_b_data_s),
      .B_addr_out             (cam_1_b_addr_s)
   );

   assign ready       = cam_0_ready_s && cam_1_ready_s;
   assign B_valid_out = cam_0_b_valid_s || cam_1_b_valid_s;
   assign B_match_out = cam_0_b_match_s || cam_1_b_match_s;
   assign B_addr_out  = (cam_0_b_match_s)? {1'd0, cam_0_b_addr_s}:{1'd1, cam_1_b_addr_s};
   assign B_data_out  = (cam_0_b_match_s)? cam_0_b_data_s : cam_1_b_data_s;

endmodule

