`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/04/2017 02:26:09 PM
// Design Name: 
// Module Name: encoder64
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module encoder64(
    input  [63:0] encoder_in,
    output reg [5:0]  binary_out
);
   always @* begin
      case (encoder_in)
         64'h0000000000000001: binary_out = 6'd0;
         64'h0000000000000002: binary_out = 6'd1;
         64'h0000000000000004: binary_out = 6'd2;
         64'h0000000000000008: binary_out = 6'd3;
         64'h0000000000000010: binary_out = 6'd4;
         64'h0000000000000020: binary_out = 6'd5;
         64'h0000000000000040: binary_out = 6'd6;
         64'h0000000000000080: binary_out = 6'd7;
         64'h0000000000000100: binary_out = 6'd8;
         64'h0000000000000200: binary_out = 6'd9;
         64'h0000000000000400: binary_out = 6'd10;
         64'h0000000000000800: binary_out = 6'd11;
         64'h0000000000001000: binary_out = 6'd12;
         64'h0000000000002000: binary_out = 6'd13;
         64'h0000000000004000: binary_out = 6'd14;
         64'h0000000000008000: binary_out = 6'd15;
         64'h0000000000010000: binary_out = 6'd16;
         64'h0000000000020000: binary_out = 6'd17;
         64'h0000000000040000: binary_out = 6'd18;
         64'h0000000000080000: binary_out = 6'd19;
         64'h0000000000100000: binary_out = 6'd20;
         64'h0000000000200000: binary_out = 6'd21;
         64'h0000000000400000: binary_out = 6'd22;
         64'h0000000000800000: binary_out = 6'd23;
         64'h0000000001000000: binary_out = 6'd24;
         64'h0000000002000000: binary_out = 6'd25;
         64'h0000000004000000: binary_out = 6'd26;
         64'h0000000008000000: binary_out = 6'd27;
         64'h0000000010000000: binary_out = 6'd28;
         64'h0000000020000000: binary_out = 6'd29;
         64'h0000000040000000: binary_out = 6'd30;
         64'h0000000080000000: binary_out = 6'd31;
         64'h0000000100000000: binary_out = 6'd32;
         64'h0000000200000000: binary_out = 6'd33;
         64'h0000000400000000: binary_out = 6'd34;
         64'h0000000800000000: binary_out = 6'd35;
         64'h0000001000000000: binary_out = 6'd36;
         64'h0000002000000000: binary_out = 6'd37;
         64'h0000004000000000: binary_out = 6'd38;
         64'h0000008000000000: binary_out = 6'd39;
         64'h0000010000000000: binary_out = 6'd40;
         64'h0000020000000000: binary_out = 6'd41;
         64'h0000040000000000: binary_out = 6'd42;
         64'h0000080000000000: binary_out = 6'd43;
         64'h0000100000000000: binary_out = 6'd44;
         64'h0000200000000000: binary_out = 6'd45;
         64'h0000400000000000: binary_out = 6'd46;
         64'h0000800000000000: binary_out = 6'd47;
         64'h0001000000000000: binary_out = 6'd48;
         64'h0002000000000000: binary_out = 6'd49;
         64'h0004000000000000: binary_out = 6'd50;
         64'h0008000000000000: binary_out = 6'd51;
         64'h0010000000000000: binary_out = 6'd52;
         64'h0020000000000000: binary_out = 6'd53;
         64'h0040000000000000: binary_out = 6'd54;
         64'h0080000000000000: binary_out = 6'd55;
         64'h0100000000000000: binary_out = 6'd56;
         64'h0080000000000000: binary_out = 6'd55;
         64'h0100000000000000: binary_out = 6'd56;
         64'h0200000000000000: binary_out = 6'd57;
         64'h0400000000000000: binary_out = 6'd58;
         64'h0800000000000000: binary_out = 6'd59;
         64'h1000000000000000: binary_out = 6'd60;
         64'h2000000000000000: binary_out = 6'd61;
         64'h4000000000000000: binary_out = 6'd62;
         64'h8000000000000000: binary_out = 6'd63;
         default: binary_out = 6'd0;
      endcase
   end
endmodule